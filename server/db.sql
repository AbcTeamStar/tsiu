CREATE TABLE users (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(12),
    uuid CHAR(36),
    sex  INTEGER
);

CREATE TABLE tsiu.room (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(10)
);

CREATE TABLE stay (
    id_user INTEGER not NULL,
    id_room INTEGER not NULL,
    time_in TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    time_out TIMESTAMP  NULL DEFAULT NULL,
    PRIMARY KEY (id_user, id_room, time_in),
    FOREIGN KEY (id_user) REFERENCES users(id),
    FOREIGN KEY (id_room) REFERENCES room(id)
);



INSERT INTO tsiu.room (name)
VALUES
('Aula A'),
('Aula B'),
('Aula C'),
('Aula D'),
('Aula E'),
('Lab 2'),
('Lab 3');